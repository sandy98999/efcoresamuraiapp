﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SamuraiApp.Domain
{
    public class SamuraiBattleStats
    {
        public string Name { get; set; }
        public int? NoOfBattles { get; set; }
        public string EarliestBattle { get; set; }
    }
}
