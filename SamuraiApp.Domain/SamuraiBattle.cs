﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SamuraiApp.Domain
{
    //Many to many relationship between Samurai and Battle
    public class SamuraiBattle
    {
        public int SamuraiId { get; set; }
        public int BattleId { get; set; }
        
    }
}
