using System;
using System.Linq;
using ConsoleApp;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SamuraiApp.Data;
using SamuraiApp.Domain;

namespace Test
{
    [TestClass]
    public class InMemoryTest
    {
        [TestMethod]
        public void CanInsertInDatabase()
        {
            var builder = new DbContextOptionsBuilder();
            builder.UseInMemoryDatabase("Test");

            using (var context=new SamuraiContext(builder.Options))
            {
                var samurai = new Samurai();
                context.Samurais.Add(samurai);
                Assert.AreEqual(EntityState.Added,context.Entry(samurai).State);
            }
        }

        [TestMethod]
        public void BusinessDataLogicAddMultipleSamurai()
        {
            var builder = new DbContextOptionsBuilder();
            builder.UseInMemoryDatabase("Test");

            using (var context = new SamuraiContext(builder.Options))
            {
                var businessLogic = new BusinessDataLogic();
                var names = new String[] {"test1", "test2", "test3"};
                var result = businessLogic.AddMultipleSamurai(names);

                Assert.AreEqual(names.Count(),result);
            }
        }
    }
}
