﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SamuraiApp.Data.Migrations
{
    public partial class AddingFunction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"CREATE Function [dbo].[EarliestBattleFought] (@SamuraiId int)
                Returns char(30) as
                begin
                DECLARE @ret char(30)
                SELECT TOP 1 @ret=Name
                FROM Battle
                WHERE Battle.Id in (
                	SELECT BattleID
                	FROM SamuraiBattle
                	Where SamuraiBattle.SamuraiId = @SamuraiId
                )
                Order by startDate
                Return @ret
                end
                "
            );
            migrationBuilder.Sql(
                @"CREATE or ALTER VIEW dbo.SamuraiBattleStats
                AS
                Select sb.SamuraiId,s.Name,count(sb.BattleId) as NoOfBattles,
                dbo.EarliestBattleFought(MIN(s.Id)) as earliestBattle
                from SamuraiBattle sb inner Join 
                dbo.Samurais s on sb.SamuraiId=s.Id
                group by s.Name,sb.SamuraiId"
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION EarliestBattleFought");
            migrationBuilder.Sql("DROP VIEW SamuraiBattleStats");
        }
    }
}
