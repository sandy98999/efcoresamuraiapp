﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SamuraiApp.Data;
using SamuraiApp.Domain;

namespace ConsoleApp
{
    class Program
    {
        private static SamuraiContext _samuraiContext = new SamuraiContext();
        static void Main(string[] args)
        {
            AddSaumrai();

            //Bulk operation has default threshold of 4, Max Row 1000 and Max Parameter 2100
            //If it is not bulk operation, then for each insert statements would be generated
            InsertBulkOperation();

            //New feature where we can insert various types in one go
            InsertVariousTypes();

            GetAllSamurai();

            //Never hardcode in query always use parameter else directly value is passed to database
            QueryFilter();

            DeleteSamurai();
            InsertBattle();

            //In disconnected all fields gets updated 
            QueryAndUpdateBattle_Disconnected();

            //Eventhough foreign key is not specified, internally sql create id and assigns foreign key
            InsertSamuraiWithQuotes();

            //use attach method instead of update, otherwise update to samurai would also happen
            AddQuoteToExistingSamurai_Disconnected();

            //It includes related objects as well, can't be used with FirstOrDefault or Find
            EagerLoadSamuraiWithQuote();

            //This will also include objects with projection select statement
            ProjectSamuraiWithQuote();

            //It can be done using collection or reference along with load method
            ExplicitLoadQuotes();

            //use entity method for this, if update is used all quote would be updated
            ModifyingRelatedDataWhenNotTracked();

            //to create a function or view, add a new empty migration and in up method migrationBuilder.Sql(@"") and in down drop this and then update database
            QuerySamuraiView();

            //Raw sql, FromSqlRaw and FromSqlInterpolated, always use interpolated if parameter is passed to prevent sql injection
            RawSamuraiQuery();
        }

        private static void RawSamuraiQuery()
        {
            //dont select less or more columns than specified in the context
            var samurai = _samuraiContext.Samurais.FromSqlRaw("select * from samurais").ToList();
        }

        private static void QuerySamuraiView()
        {
            var data = _samuraiContext.SamuraiBattleStatses.ToList();
        }

        private static void ModifyingRelatedDataWhenNotTracked()
        {
            var samurai = _samuraiContext.Samurais.Include(s => s.Quotes).FirstOrDefault(s => s.Id == 2);
            var quote = samurai.Quotes[0];
            quote.Text = "I changed text";
            using (var context = new SamuraiContext())
            {
                context.Entry(quote).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        private static void ExplicitLoadQuotes()
        {
            var samurai = _samuraiContext.Samurais.FirstOrDefault();
            _samuraiContext.Entry(samurai).Reference(s => s.Horse).Load(); //for one record
            _samuraiContext.Entry(samurai).Collection(s => s.Quotes).Load(); //for list
        }

        private static void ProjectSamuraiWithQuote()
        {
            var samurai = _samuraiContext.Samurais
                .Select(s => new
                {
                    Id = s.Id,
                    Quotes = s.Quotes
                })
                .ToList();
        }

        private static void EagerLoadSamuraiWithQuote()
        {
            var samurai = _samuraiContext.Samurais.Include(x => x.Quotes).ToList();
        }

        private static void AddQuoteToExistingSamurai_Disconnected()
        {
            var samurai = _samuraiContext.Samurais.FirstOrDefault();
            samurai.Quotes.Add(new Quote
            {
                Text = "I am disconnected quote"
            });

            using (var context=new SamuraiContext())
            {
                context.Samurais.Attach(samurai);
                context.SaveChanges();
            }
        }

        private static void InsertSamuraiWithQuotes()
        {
            var samurai = new Samurai()
            {
                Name = "Kyuo",
                Quotes = new List<Quote>()
                {
                    new Quote(){Text = "I am testing"},
                    new Quote(){Text = "I am also testing"}
                }
            };

            _samuraiContext.Samurais.Add(samurai);
            _samuraiContext.SaveChanges();
        }

        private static void QueryAndUpdateBattle_Disconnected()
        {
            var battle = _samuraiContext.Battle.AsNoTracking().FirstOrDefault();
            battle.EndDate = new DateTime(1560,06,30);

            using (var context=new SamuraiContext())
            {
                context.Battle.Update(battle);
                context.SaveChanges();
            }
        }

        private static void InsertBattle()
        {
            var battle=new Battle()
            {
                Name = "Battle of Oke",
                StartDate = new DateTime(1560,05,01),
                EndDate = new DateTime(1560,06,15),
            };

            _samuraiContext.Battle.Add(battle);
            _samuraiContext.SaveChanges();
        }

        private static void DeleteSamurai()
        {
            var samurai = _samuraiContext.Samurais.Find(5);
            _samuraiContext.Remove(samurai);
            _samuraiContext.SaveChanges();
        }

        private static void QueryFilter()
        {
            var name = "kiku";
            var samurai = _samuraiContext.Samurais.Where(x => x.Name == name).ToList();

            //we can use like as well, contains is internally converted into like
            var samuraiName = _samuraiContext.Samurais.Where(x => EF.Functions.Like("kiku", x.Name)).ToList();

            //it is compulsory to order by for this  
            var last = _samuraiContext.Samurais.OrderBy(o => o.Id).LastOrDefault();
        }

        private static void InsertVariousTypes()
        {
            var samurai = new Samurai() { Name = "Kiku" };
            var clan = new Clan() { ClanName = "Imperial Clan" };

            //Note: we are doing operation directly on context
            _samuraiContext.AddRange(samurai,clan);
            _samuraiContext.SaveChanges();
        }

        private static List<Samurai> GetAllSamurai()
        {
            var samurais = _samuraiContext.Samurais.ToList();
            return samurais;
        }

        private static void InsertBulkOperation()
        {
            var samurais = new List<Samurai>()
            {
                new Samurai() {Name = "Number1"},
                new Samurai() {Name = "Number2"},
                new Samurai() {Name = "Number3"},
                new Samurai() {Name = "Number4"},
            };

            _samuraiContext.Samurais.AddRange(samurais);
            _samuraiContext.SaveChanges();
        }

        private static void AddSaumrai()
        {
            var samurai = new Samurai() { Name = "Sampson" };
            _samuraiContext.Samurais.Add(samurai);
            _samuraiContext.SaveChanges();
        }
    }
}
