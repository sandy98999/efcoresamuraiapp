﻿using System;
using System.Collections.Generic;
using System.Text;
using SamuraiApp.Data;
using SamuraiApp.Domain;

namespace ConsoleApp
{
    public class BusinessDataLogic
    {
        private readonly SamuraiContext _context;

        public BusinessDataLogic(SamuraiContext context)
        {
            _context = context;
        }
        public BusinessDataLogic()
        {
            _context = new SamuraiContext();
        }


        public int AddMultipleSamurai(string[] names)
        {
            var samuraiList = new List<Samurai>();
            foreach (var name in names)
            {
                samuraiList.Add(new Samurai{ Name = name});
            }

            _context.Samurais.AddRange(samuraiList);
            var dbResult = _context.SaveChanges();
            return dbResult;
        }
    }
}
